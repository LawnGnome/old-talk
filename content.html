<!DOCTYPE HTML>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>What PHP learned from Python</title>

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
	</head>

	<body>
		<div class="reveal">
			<div class="slides">
				<section id="title">
					<h1>What <img class="php" src="images/php.svg" alt="PHP"> learned from <img class="python" src="images/python.svg" alt="Python"></h1>
					<h2><a href="https://about.me/LawnGnome">Adam Harvey</a></h2>
					<h4>(he/him/his)</h4>
					<h3 class="twitter"><a href="https://twitter.com/LGnome"><img class="twitter" src="images/twitter.svg" alt="Twitter">@LGnome</a></h3>
					<h3 class="nr"><a href="https://newrelic.com/"><img src="images/nr.svg" alt="New Relic"></a></h3>
					<aside class="notes">
						Hello! My name is Adam, and I'm here to talk about what PHP learned
						from Python: specifically, the Python 2 to 3 transition. I am a
						somewhat lapsed member of the PHP internals team, which is
						basically the equivalent of the Python core developers group. I've
						also written a lot of Python over the years: two of my last four
						jobs have been primarily Python based, so I'm hopefully not a total
						fraud up here.
					</aside>
				</section>

				<section id="php">
					<section id="what-is-php">
						<img src="images/php.svg" alt="PHP">
						<aside class="notes">
							This talk isn't really about PHP &mdash; indeed, it's barely
							about Python, in truth &mdash; but I'll provide some quick
							background for those who aren't familiar with it. For those of
							you who don't know PHP, it is the world's leading programming
							language for implementing blogging platforms with security
							issues. It also drives a truly astonishing percentage of the Web.
						</aside>
					</section>

					<section id="php-versioning">
						<ul>
							<li><span class="php">PHP</span>/FI</li>
							<li><span class="php">PHP</span>/FI 2</li>
							<li><span class="php">PHP</span><span class="slash">/</span>3</li>
							<li><span class="php">PHP</span><span class="slash">/</span>4</li>
							<li><span class="php">PHP</span><span class="slash">/</span>5</li>
							<li class="nope"><span class="php">PHP</span><span class="slash">/</span>6</li>
							<li><span class="php">PHP</span><span class="slash">/</span>7</li>
						</ul>
						<aside class="notes">
							PHP features an unusual versioning system based on octal with
							modifications. Most importantly, note that there is no 6. PHP
							made the jump from version 5 to version 7 about three years ago
							&mdash; like Python 3, PHP 7 is an attempt to clean up some of
							the wartier parts of the language and runtime while hopefully
							bringing users along for the ride.
						</aside>
					</section>

					<section id="php-versioning-faded">
						<ul>
							<li class="fade"><span class="php">PHP</span>/FI</li>
							<li class="fade"><span class="php">PHP</span>/FI 2</li>
							<li class="fade"><span class="php">PHP</span><span class="slash">/</span>3</li>
							<li class="fade"><span class="php">PHP</span><span class="slash">/</span>4</li>
							<li><span class="php">PHP</span><span class="slash">/</span>5</li>
							<li><span class="arrow">&darr;</span><span class="php">PHP</span><span class="slash">/</span><span class="nope fade">6</span></li>
							<li><span class="php">PHP</span><span class="slash">/</span>7</li>
						</ul>
						<aside class="notes">
							I was pretty involved with the internals team at the time the PHP
							7 transition was being planned, and one of the things that was
							brought up often was how the Python 3 transition &mdash; then
							about seven years along &mdash; had gone, and what we could learn
							from it.
						</aside>
					</section>
				</section>

				<section id="holistic-view">
					<section id="bliss" data-background="images/bliss.jpg">
						<aside class="notes">
							OK, so let's talk on a holistic level about upgrades, and then
							dive into some examples. As developers, we tend towards the big
							change. The rewrite. The green fields.
						</aside>
					</section>

					<section id="forest" data-background="images/forest.jpg">
						<aside class="notes">
							But if a language upgrades in the forest and nobody can or will
							update, does the new version really exist? It doesn't matter how
							awesome it is, or clean: if it's as hard to go from version 5 to
							version 6 as it would be to migrate to a different, more
							established, and probably better supported tool, why wouldn't
							your users do that?
						</aside>
					</section>

					<section id="sticks" data-background="images/bats.jpg">
						<aside class="notes">
							So you end up with a balancing act. I tend to think of it as
							trying to mix the carrot with the stick. The stick is really
							anything that breaks anything. It doesn't matter how well founded
							the change is: change is scary, and you've just made work for a
							lot of people.
						</aside>
					</section>

					<section id="carrots" data-background="images/carrots.jpg">
						<aside class="notes">
							Since most people don't want to incur pain for no gain, you also
							need a carrot. Preferably lots of them. A veritable field.
						</aside>
					</section>

					<section id="bc">
            <svg class="bc" viewBox="0 0 1000 500">
              <defs>
                <marker id="arrowhead" markerWidth="4" markerHeight="4" orient="auto" refY="2">
                  <path d="M0,0 L4,2 0,4" />
                </marker>
              </defs>

              <circle class="old fill" cx="350" cy="210" r="200" />
              <circle class="new fill" cx="650" cy="210" r="200" />
              <circle class="outline" cx="350" cy="210" r="200" />
              <circle class="outline" cx="650" cy="210" r="200" />
              <text class="label" x="310" y="200">Old &amp;</text>
              <text class="label" x="310" y="260">broken</text>
              <text class="label" x="690" y="200">New &amp;</text>
              <text class="label" x="690" y="260">shiny</text>
              <text class="label" x="500" y="490">Libraries</text>
              <line class="arrow" x1="500" y1="450" x2="500" y2="250" />
            </svg>
						<aside class="notes">
							There's also one other consideration: your most important users
							have to both avoid the sticks <em>and</em> can't enjoy the
							carrots. In the case of languages, this is mostly library and
							framework developers. They have to live in that little piece in
							the middle by writing code that either works on both versions or
							coming up with clever ways to have version specific code. Both
							suck. It needs to not suck as much as possible. And the best way
							to make it not suck is to make that sliver as wide as possible.
						</aside>
					</section>
				</section>

				<section id="python-intro">
					<h2>Py3k</h2>
					<aside class="notes">
						So, let's apply this to Python 3. Since I'm a bright, sunny sort of
						a person, let's start with the sticks. What made it hard to upgrade
						from Python 2 to Python 3, particularly in the earlier days of 3?
					</aside>
				</section>

				<section data-background="images/strings.jpg">
					<section id="strings" data-background="images/strings.jpg">
						<aside class="notes">
							Oh. Oh no.
						</aside>
					</section>

					<section id="string-examples" data-background="translucent">
						<pre><code class="python" data-trim>
unicode("foo")
u"foo"
"\xab\xcd"

str("foo")
"foo"
b"\xab\xcd"
						</code></pre>
						<aside class="notes">
							Strings are a pernicious nest of stringy things. Like snakes, I
							guess. They're also fundamental to an amazing number of tasks
							that people want to use languages like Python and PHP for. And
							Python 3 significantly changed the semantics of how you declared
							them, manipulated them, and used them.
						</aside>
					</section>

					<section id="two-to-three" data-background="translucent">
						<pre><code class="python" data-trim>
from sys import argv

print "Hello %s" % (unicode(argv[1]))
						</code></pre>
						<div>&darr;</div>
						<pre><code class="python" data-trim>
from sys import argv

print("Hello %s" % (str(argv[1])))
						</code></pre>
						<aside class="notes">
							The intention at the time was that well formed, modern Python 2.6
							code could be transformed using 2to3. The problem is that I
							didn't know anyone at the time who was exclusively writing well
							formed, modern Python 2.6 code, and <em>definitely</em> none of
							the major frameworks and libraries were, since they always had to
							support the previous few years' worth of Python versions.
						</aside>
					</section>

					<section id="u-prefix" data-background="translucent">
						<pre><code class="python" data-trim>
u"foo"
						</code></pre>
						<aside class="notes">
							Obviously, this wasn't insurmountable. Tooling eventually
							improved, particularly with the rise of six, and once you could
							drop Python 2.5 support it wasn't really that bad. But for one
							problem: Python 3 dropped Unicode string literals, since they
							were technically useless in Python 3. But doing so made it more
							annoying to write code that supported both versions. This
							oversight wasn't corrected until Python 3.3, years after Python
							3.0 was released.
						</aside>
					</section>

					<section id="u-prefix-two" data-background="translucent">
						<pre><code class="python" data-trim>
u"foo"
						</code></pre>
						<aside class="notes">
							That's a good example of narrowing the intersecting circles, and
							it was for no real reason. The PEP to reinstate the literal noted
							that this was a problem for porting projects such as Pyramid,
							Werkzeug, Django, Flask, and perhaps most topically in the last
							week, Mercurial. Those projects underpin so many other projects
							that you immediately had a chicken and egg problem: who was going
							to port their application if their framework didn't work?
						</aside>
					</section>

					<section id="php-string-lesson" data-background="translucent">
						<pre><code class="php" data-trim>
&lt;?php

$foo = b"foo";
						</code></pre>
						<aside class="notes">
							The funny part, though, is that the lesson PHP learned was a bit
							weirder than this, which is maybe a good example in learning why
							a lesson is valuable rather than necessarily the lesson itself..
							PHP only has one string type: it's a true binary string, and PHP
							doesn't track (or care) what encoding your string is. But that
							wasn't always going to be the case: around the same time Python 3
							was taking its first steps into the world, the PHP project was
							developing version 6, even though the modified octal numbering
							scheme didn't permit it.
						</aside>
					</section>

					<section id="php-b-strings" data-background="translucent">
						<pre><code class="php" data-trim>
&lt;?php

$foo = b"foo";
						</code></pre>
						<aside class="notes">
							This would have worked similarly to the Python change: binary
							strings would be a new type, and a new b-string literal would
							have been added. To ease adoption, like Python 2.6, PHP 5.3
							speculatively included support for these literals.
						</aside>
					</section>

					<section id="php-b-strings-continued" data-background="translucent">
						<pre><code class="php" data-trim>
&lt;?php

$foo = b"foo";
						</code></pre>
						<aside class="notes">
							Except, in the end, PHP 6 never happened. The underlying
							implementation was slow and used UTF-16, but more damningly, it
							turned out that nobody really cared: in the real world of PHP
							users, everyone just made everything UTF-8 and went on with their
							lives. The fact that you might accidentally slice a UTF-8
							codepoint in the middle even turned out to not be a big deal for
							most actual users.
						</aside>
					</section>

					<section id="php-b-strings-today" data-background="translucent">
						<pre><code class="sh" data-trim>
$ php -v
PHP 7.4.2 (cli) (built: Jan 21 2020 18:16:58) ( NTS )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
    with Zend OPcache v7.4.2, Copyright (c), by Zend Technologies
$ php -r 'var_dump(b"foo");'
string(3) "foo"</code></pre>
						<aside class="notes">
							But, if you run PHP 7.3 today with a b-string, it still works.
							And one of the reasons for this is that people <em>did</em> port
							their code to use the syntax, and we learned from Python that
							breaking that was a really annoying sliver for a lot of people.
						</aside>
					</section>
				</section>

				<section data-background="images/overflow.jpg">
					<section id="iteration" data-background="translucent">
						<pre><code class="python" data-trim>
a = range(0, 100000000)
						</code></pre>
						<aside class="notes">
							Another example of this was the various changes that occurred
							around iteration. Basically, in Python 3, lots of things that
							returned real lists were changed to return iterators or views,
							which was great, particularly as somebody who, on a Python 2 site
							as a new developer at a research centre, once naïvely wrote code
							that called range() with a large maximum value on a shared
							server. Don't do that.
						</aside>
					</section>

					<section id="iteritems" data-background="translucent">
						<pre><code class="python" data-trim>
for f in d.iteritems():
    do_something_awesome(f)
						</code></pre>
						<div>&darr;</div>
						<pre><code class="python" data-trim>
for f in d.items():
    do_something_awesome(f)
						</code></pre>
						<aside class="notes">
							Again, though, a bunch of methods that were used to opt into
							iterators on Python 2 were removed on Python 3.  This was
							excellent from the point of view of readability, but it meant
							that your options were to write code that had subpar performance
							on Python 2, or you had to write shims to invoke, say,
							iteritems() instead of items() on a version dependent basis.
						</aside>
					</section>

					<section id="assert" data-background="translucent">
						<pre><code class="php" data-trim>
&lt;?php

strpos($haystack, $needle);
stristr($haystack, $needle);
in_array($needle, $haystack);
array_search($needle, $haystack);
						</code></pre>
						<aside class="notes">
							The lesson here was pretty simple for us: even though it would
							have been nice to clean up some of the horrible warts in the
							standard library around argument ordering and naming, we
							basically kept all of them with new aliases where we had better
							options. Indeed, we even kept the old behaviour of the assert()
							function, even as we turned it into a language construct with
							totally different semantics &mdash; but they were semantics you
							had to opt into, and we (the royal we) spent a lot of time
							preserving the old ones. So, you get this beautiful piece of
							consistency: but it means that code from 1998 still mostly works,
							for better or worse.
						</aside>
					</section>
				</section>

				<section>
					<section id="future" data-background="translucent">
						<pre><code class="python" data-trim>
from __future__ import crystal_ball
						</code></pre>
						<aside class="notes">
							Future imports were an interesting approach that I certainly
							looked at using in later PHP 5 versions. Conceptually, they're
							simple: opt in early for new functionality.
						</aside>
					</section>

					<section id="division" data-background="translucent">
						<pre><code class="python" data-trim>
a = 1 / 2
a = 1 // 2
						</code></pre>
						<aside class="notes">
							The catch, though, is that they're easily ignored. The behaviour
							that's going away is something developers have to be aware of,
							rather than being notified of. And they're a crutch that users
							can lean on whereby they get a lot of the newer features without
							actually performing the migration you're trying to convince them
							is worth it.
						</aside>
					</section>

					<section id="" data-background="translucent">
						<pre><code class="python" data-trim>
a = 1 / 2
a = 1 // 2
						</code></pre>
						<aside class="notes">
							So, being PHP, we did the opposite. We warned users about things
							that were going to change. Loudly. And didn't let them opt in,
							but instead told them about what was going to change.
						</aside>
					</section>

					<section id="complain">
						<pre><code class="python" data-trim>
from __future__ import complain
						</code></pre>
						<aside class="notes">
							I'm not hating on from future, which I think is also a very
							elegant solution. But you can't rely on users to keep up with
							release notes: lots of developers are busy, or uninterested, or
							just want to get their thesis written. If you don't tell them 1
							divided by 2 is going to change, they really might not pay
							attention. Be loud.
						</aside>
					</section>
				</section>

				<section data-background="images/more-carrots.jpg">
					<section id="pythonic-carrots" data-background="images/more-carrots.jpg">
						<aside class="notes">
							Finally, let's talk about carrots.
						</aside>
					</section>

					<section id="porting" data-background="translucent">
						<div class="source">
							<a href="https://hynek.me/articles/python3-2016/">
								Hynek Schlawack
							</a>
						</div>
						<img src="images/market.png">
						<aside class="notes">
							I don't think it will shock too many people if I suggest that
							Python 3 adoption was pretty slow. Part of that was that porting
							code wasn't as straightforward as it could have been. But the
							other problem was that there weren't really enough reasons to
							port the code. (Yes, I know these numbers have improved since
							2016.)
						</aside>
					</section>

					<section id="new-stuff" data-background="translucent">
						<div class="source">
							<a href="https://docs.python.org/3/whatsnew/3.0.html">
								What's New in Python 3.0
							</a>
						</div>
						<img src="images/py3k.png">
						<aside class="notes">
							One of the things that was surprising, looking back at the Python
							3.0 release notes, was how little new stuff there actually was.
							The improved integer type was a definite win, for sure, the new
							string semantics were obviously going to be better in the long
							run, exception handling was certainly improved, and I was never
							going to have to remember the order of arguments for super() ever
							again... but honestly, most of the the interesting stuff was in
							Python 2.6 or 2.7 already, either hidden behind future imports or
							just straight up backported. And you could use that without
							porting your code.
						</aside>
					</section>

					<section id="years" data-background="translucent">
						<div class="source">
							<a href="https://docs.python.org/3/whatsnew/3.0.html">
								What's New in Python 3.0
							</a>
						</div>
						<img src="images/py3k.png">
						<aside class="notes">
							If I think about the things about Python 3 that make my life
							better today, most of them actually came years after Python 3 was
							released. Ordered dictionaries were in 3.1. Argparse changed my
							life, but was in 3.2. Yield from was in 3.3. asyncio in 3.4. And
							so on. (Python 3.7 has breakpoint(), which I learned yesterday
							from Nina, so the process continues.)
						</aside>
					</section>

					<section id="twenty-fourteen" data-background="translucent">
						<div class="source">
							<a href="https://web.archive.org/web/20140309041750/http://py3readiness.org/">
								Python 3 Readiness
							</a>
						</div>
						<img src="images/2014.png" style="height: 10em">
						<div>March 2014</div>
						<aside class="notes">
							For most developers I know, the pain of porting only really
							became worth it around the 3.3 timeframe. And that still didn't
							take into account waiting for their dependencies to be ported
							&mdash; I suspect that those authors had the same thought
							process, since in March 2014 (as 3.4 was being released), only
							two thirds of the top 360 Python packages had been ported.
							Admittedly, this was biased towards the lower part of the list,
							but that's a long tail of things, and in a lot of cases, you only
							needed to depend on one of them.
						</aside>
					</section>

					<section id="twenty-nineteen" data-background="translucent">
						<div class="source">
							<a href="http://py3readiness.org/">
								Python 3 Readiness
							</a>
						</div>
						<img src="images/2019.png" style="height: 10em">
						<div>January 2020</div>
						<aside class="notes">
							At the end of the day, if you're going to move people's cheese,
							you <em>have</em> to provide a good enough reason for them to
							chase it. Python 3 eventually did, but I'd argue Python 3.0 to
							3.(mumble) didn't. (This is from py3readiness.org.)
						</aside>
					</section>

					<section id="contributors" data-background="translucent">
						<div>PHP</div>
						<img src="images/php-contributors.png">
						<div>Python</div>
						<img src="images/python-contributors.png">
						<aside class="notes">
							For PHP, we wanted a quicker migration path.  Open source
							language runtimes tend to have smaller teams than you think.
							(This is just as true for PHP as it is for Python.) Trying to
							maintain PHP 5 for more than a couple of years of overlap was
							likely going to stretch us further than we could go.  So, in
							spite of years of discussion, PHP 7 didn't happen until we had a
							truly compelling story. In our case, it was around performance.
							For other projects, maybe it would be features, or security, or
							something else that people value. But it has to be present.
							(Incidentally, one interesting thing on this slide is that the
							Python contributors number has gone up a tonne in the last nine
							months: it's fully 250 more than it was when I first gave this
							talk in November.)
						</aside>
					</section>

					<section id="progress-report" data-background="images/php-versions.png">
						<div class="source">
							<a href="https://seld.be/notes">Jordi Boggiano (Packagist)</a>
						</div>
						<aside class="notes">
							I'm pleased to report that PHP 7 is a success by most metrics,
							and has had remarkably good uptake from the PHP community. And I
							think it's fair to say that we couldn't have done it without
							Python taking its own big step into the world of Python 3 a
							decade ago.
						</aside>
					</section>

					<section id="takeaways" data-background="translucent">
						<ul>
							<li>Break things cautiously</li>
							<li>Maintain terrible things if it makes life better</li>
							<li>Expand the zone of overlap</li>
						</ul>
						<aside class="notes">
							So, if there's one takeaway I want to finish on, it's this: break
							things with great caution. Maintain things, even if they're
							obsolete, if they'll help your most important users stay in that
							happy overlap zone where they can support both versions for as
							long as they need to. Those are lessons that we learned from a
							successful project: Python 3.
						</aside>
					</section>
				</section>

				<section id="thanks">
					<h2>Thanks!</h2>
					<h3 class="twitter"><a href="https://twitter.com/LGnome"><img class="twitter" src="images/twitter.svg" alt="Twitter">@LGnome</a></h3>
					<h3>Slides: <a href="https://lawngnome.github.io/what-php-learned-from-python/">lawngnome.github.io/what-php-learned-from-python</a></h3>
					<aside class="notes">
						I will tweet out the link to the slides. Unless I already have, in
						which case good for me.
					</aside>
				</section>
			</div>
		</div>

	</body>
</html>
<!-- vim: set nocin ai noet ts=2 sw=2: -->
